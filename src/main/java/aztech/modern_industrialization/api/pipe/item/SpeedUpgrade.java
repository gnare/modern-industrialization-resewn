/*
 * MIT License
 *
 * Copyright (c) 2020 Azercoco & Technici4n
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package aztech.modern_industrialization.api.pipe.item;

import aztech.modern_industrialization.MIItem;
import aztech.modern_industrialization.compat.kubejs.KubeJSProxy;
import java.util.HashMap;
import java.util.Map;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;

/**
 * A speed upgrade for an item pipe
 */
public class SpeedUpgrade {

    public final static Map<ResourceLocation, Long> UPGRADES = new HashMap<>();

    static void registerUpgrade(Item item, long speed) {
        UPGRADES.put(BuiltInRegistries.ITEM.getKey(item), speed);
    }

    static {
        registerUpgrade(MIItem.MOTOR.asItem(), 2L);
        registerUpgrade(MIItem.LARGE_MOTOR.asItem(), 8L);
        registerUpgrade(MIItem.ADVANCED_MOTOR.asItem(), 32L);
        registerUpgrade(MIItem.LARGE_ADVANCED_MOTOR.asItem(), 64L);

        KubeJSProxy.instance.fireRegisterMotorsEvents();
    }

}
